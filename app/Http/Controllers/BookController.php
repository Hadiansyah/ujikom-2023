<?php
namespace App\Http\Controllers;
use App\Models\Book;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function index()
    {
        $data = book; ;all();
        return view('Book.Index', ['data' => $data]);
    }
    public function tambah(Request $request)
    {
        DB::table('Books')->insert([
            'name' => $request->name  
        ]);
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = Book::all()->where('id', $id);
        return view('Book.edit', ['data' => $data]);
    }
    public function update(Request $request)
    {
        DB::table('Books')->where('id', $request->id)->update([
            'name' => $request->name
        ]);
        return redirect('/Book');
    }
}
