<?php
namespace App\Http\Controllers;
use App\Models\Komputer;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function index()
    {
        $data = Komputer; ;all();
        return view('Komputer.Index', ['data' => $data]);
    }
    public function tambah(Request $request)
    {
        DB::table('Komputer')->insert([
            'name' => $request->name  
        ]);
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = Komputer::all()->where('id', $id);
        return view('Komputer.edit', ['data' => $data]);
    }
    public function update(Request $request)
    {
        DB::table('Komputer')->where('id', $request->id)->update([
            'name' => $request->name
        ]);
        return redirect('/Komputer');
    }
}
