@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
        <div class="card-body">
            @if (Auth()->user()->level = 'admin')
                <a href="/createAccount" class="btn btn-primary">Tambah Siswa</a>
            @endif
            <table class="table table-inverse table-inverse table-responsive text-center">
                <thead class="thead-inverse|thead-default">
                    <tr >
                        <th class="text-center">NIS</th>
                        <th class="text-center">Nama Lengkap</th>
                        <th class="text-center">Nama Pembimbing</th>
                        <th class="text-center">Progress</th>
                        <th class="text-center">aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td scope="row">{{ $item->nis }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @if ( $item->pembimbing_id == null)
                                        Belum memiliki pembimbing
                                    @else
                                        
                                    {{ $item->pembimbing->name }}
                                    @endif
                                </td>
                                <td>
                                @if ($item->status == null)
                                    Siswa belum memulai progress
                                @else
                                    {{ $item->status }}
                                    
                                @endif
                                </td>
                                <td>
                                    @if (Auth()->User()->level == 'pembimbing')
                                        
                                        @if ( $item->status == 'Menunggu Proses Validasi Surat Pengantar')
                                            <a href="/validasiPengantar/{{ $item->id }}" class="btn btn-info">Validasi Surat Pengantar</a>
                                        @elseif ($item->status == 'Lembar Pengesahan di terima')
                                            <button type="button" class="btn btn-primary">Lembar pengesahan Di terima</button>
                                        @elseif ($item->status == 'Masa Sanggah balasan surat Pengantar/pegajuan')
                                            <a href="/konfirmasiPengajuan/{{ $item->id }}" class="btn btn-info">Konfirmasi Surat Pengantar</a>
                                        @elseif ($item->status == null)
                                            <button type="button" class="btn btn-danger">Siswa belum memulai progress</button>
                                    @endif
                                    @endif
                                    <a href="/detail/{{ $item->id }}" class="btn btn-info">Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="/home" class="btn btn-secondary">Back</a>
            
        </div>
        </div>
     </div>
@endsection